import React from 'react';
import './Portfolio.css'

function Portfolio () {
    return <>
    <div className='container'>
        <div className='row head'>
            <div className='col-lg-8 col-sm-12'>
                <h1>Pankaj Kori</h1>
                <h4>Aspiring web developer</h4>
                <div className='personal-details'>
                    <p>koripankaj61@gmail.com</p>
                    <p>+91 9967972311</p>
                    <p>linkedin.com/in/pankaj-kori-286794126</p>
                    <p>Mumbai, India</p>

                </div>
                
            </div>
            <div className='col-lg-4 col-sm-12 profile-pic' >
                <img  id='profile-image' src='assets/pankaj-pic.jpeg'/>
            </div>

        </div>
        <div className='row about'>
            <h4 className='about-me-heading'>About me:</h4>
            <div className='col-lg-12'><p>Quality-focused and results-driven professional seeking a position as a Software/Web developer with JavaScript Juice where I can use my skill-set and abilities to enhance the user experience. I had continued working in Solar Industry for more than 3 years. But I wanted to explore more and grab the opportunities in IT world. So, I decided to opt-in for web development and pursue my career in it.</p>
</div>
                
        </div>

        <div className='row justify-content-around'>
        <h4 className='about-me-heading'>Personal Projects</h4>
            <div className='col-lg-3 project-details '>
                <img id='slack' src='assets/slack.webp' alt='slack-image'/>
                <p>Web Api <br/> Integration</p>
                <div className='project-details-descriptions'>Integrated Slack web-api services in nodejs and built apis to use various services in Slack application.
Send message, Update message, Delete message, Send scheduled message, Invite Member/Members, Remove member/members, Upload files</div>
            </div>
            <div className='col-lg-3 project-details '>
            <img src='assets/QR-code.png' alt='slack-image'/>
            <p>QR Code <br/> Generator</p>
            <div className='project-details-descriptions'>Built a QR-Code Generator where we can simply just input any text or url and convert that content in QR-Code
Link to access :- https://qr-code-generator.pankajkori1.repl.co</div>

            </div>
            <div className='col-lg-3 project-details '>
            <img src='assets/expense-tracker.png' alt='slack-image'/>
            <p>Expense<br/> Tracker</p>
            <div className='project-details-descriptions'>Built this Expense Tracker using React,which can simply help you keep a track of your daily and monthly expense easily.
link to access :- https://easy-expensetracker.netlify.app</div>

            </div>
        </div>
        
        <div className='row education'>
            <h4 className='about-me-heading'>Education & Skills</h4>
            <p className='education-degree'>Bachelor of Engineering</p>
            <div className='skill-icons'>
            <img src='assets/javascript.png'></img>
            <img src='assets/react.png'></img>
            <img src='assets/nodejs-development-services.webp'></img>
            <img src='assets/mysql.svg'></img>
            <img src='assets/html.png'></img>
            <img src='assets/css.png'></img>
            </div>
        </div>

        <div className='footer'>
            <p>~Build by Pankaj Kori</p>
        </div>

    </div>
    </>
}

export default Portfolio;